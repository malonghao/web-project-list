package com.itmlh.service.store;

import com.github.pagehelper.PageInfo;
import com.itmlh.domain.store.Company;

import java.util.List;

public interface CompanyService {
    /**
     *  添加
     * @param company
     * @return
     */
    void save(Company company);

    /**
     * 删除
     * @param company
     * @return
     */
    void delete(Company company);

    /**
     * 修改
     * @param company
     * @return
     */
    void update(Company company);

    /**
     * 查询单个
     * @param id
     * @return
     */
    Company findById(String id);

    /**
     * 查询所有
     * @return
     */
    List<Company> findAll();

    /**
     * 分页查询
     * @param page
     * @param size
     * @return
     */
    PageInfo findAll(int page,int size);

}
