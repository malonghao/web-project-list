package com.itmlh.service.store.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itmlh.dao.store.CompanyDao;
import com.itmlh.domain.store.Company;
import com.itmlh.factory.MapperFactory;
import com.itmlh.service.store.CompanyService;
import com.itmlh.utils.TransactionUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.List;
import java.util.UUID;

public class CompanyServiceImpl implements CompanyService {
    @Override
    public void save(Company company) {
        SqlSession sqlSession=null;
        try{
            //获取session对象
           sqlSession = MapperFactory.getSqlSession();
            //获取dao
            CompanyDao companyDao = MapperFactory.getMapper(sqlSession, CompanyDao.class);
            //id使用UUID生成策略
            String id = UUID.randomUUID().toString();
            company.setId(id);
            //调用dao层操作
            companyDao.save(company);
            //提交事务
            TransactionUtil.commit(sqlSession);
        }catch (Exception e){
            TransactionUtil.rollback(sqlSession);
            throw new RuntimeException(e);
        }finally{
            try{
                TransactionUtil.close(sqlSession);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }

    @Override
    public void delete(Company company) {
        SqlSession sqlSession=null;
        try{
            //获取session对象
            sqlSession = MapperFactory.getSqlSession();
            //获取dao
            CompanyDao companyDao = MapperFactory.getMapper(sqlSession, CompanyDao.class);
            //id使用UUID生成策略
            String id = UUID.randomUUID().toString();
            company.setId(id);
            //调用dao层操作
            companyDao.delete(company);
            //提交事务
            TransactionUtil.commit(sqlSession);
        }catch (Exception e){
            TransactionUtil.rollback(sqlSession);
            throw new RuntimeException(e);
        }finally{
            try{
                TransactionUtil.close(sqlSession);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void update(Company company) {
        SqlSession sqlSession=null;
        try{
            //获取session对象
            sqlSession = MapperFactory.getSqlSession();
            //获取dao
            CompanyDao companyDao = MapperFactory.getMapper(sqlSession, CompanyDao.class);
            //id使用UUID生成策略
            String id = UUID.randomUUID().toString();
            company.setId(id);
            //调用dao层操作
            companyDao.update(company);
            //提交事务
            TransactionUtil.commit(sqlSession);
        }catch (Exception e){
            TransactionUtil.rollback(sqlSession);
            throw new RuntimeException(e);
        }finally{
            try{
                TransactionUtil.close(sqlSession);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public Company findById(String id) {
        SqlSession sqlSession=null;
        try{
            //获取session对象
            sqlSession = MapperFactory.getSqlSession();
            //获取dao
            CompanyDao companyDao = MapperFactory.getMapper(sqlSession, CompanyDao.class);

            //调用dao层操作
            return  companyDao.findById(id);
            //提交事务

        }catch (Exception e){
            TransactionUtil.rollback(sqlSession);
            throw new RuntimeException(e);
            //日志
        }finally{
            try{
                TransactionUtil.close(sqlSession);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }

    @Override
    public List<Company> findAll() {
        SqlSession sqlSession=null;
        try{
            //获取session对象
            sqlSession = MapperFactory.getSqlSession();
            //获取dao
            CompanyDao companyDao = MapperFactory.getMapper(sqlSession, CompanyDao.class);

            //调用dao层操作
            return  companyDao.findAll();
            //提交事务

        }catch (Exception e){
            TransactionUtil.rollback(sqlSession);
            throw new RuntimeException(e);
            //日志
        }finally{
            try{
                TransactionUtil.close(sqlSession);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }

    @Override
    public PageInfo findAll(int page, int size) {
        SqlSession sqlSession=null;
        try{
            //获取session对象
            sqlSession = MapperFactory.getSqlSession();
            //获取dao
            CompanyDao companyDao = MapperFactory.getMapper(sqlSession, CompanyDao.class);

            //调用dao层操作
            PageHelper.startPage(page,size);
            List<Company> all = companyDao.findAll();
            PageInfo pageInfo=new PageInfo(all);
            return pageInfo;

        }catch (Exception e){
            TransactionUtil.rollback(sqlSession);
            throw new RuntimeException(e);
            //日志
        }finally{
            try{
                TransactionUtil.close(sqlSession);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }
}
